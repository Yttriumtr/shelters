package cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Adopter;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Dog;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Shelter;


import java.time.LocalDate;

public class DogDto implements DomainDto<Dog> {
    private String name;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate birth;
    private String gender;
    private String breed;
    private Shelter shelter;
    private String size;
    private int dogsEnergy;
    public DogDto(String name, LocalDate birth, String gender, String breed, Shelter shelter, String size, int dogsEnergy) {
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.breed = breed;
        this.shelter = shelter;
        this.size = size;
        this.dogsEnergy = dogsEnergy;
    }
    @Override
    public Dog map () {return new Dog(this.name,this.birth,this.gender,this.breed, this.shelter,this.size,this.dogsEnergy);}
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Shelter getShelter() {
        return shelter;
    }

    public void setShelter(Shelter shelter) {
        this.shelter = shelter;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getDogsEnergy() {
        return dogsEnergy;
    }

    public void setDogsEnergy(int dogsEnergy) {
        this.dogsEnergy = dogsEnergy;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}
