package cz.cvut.fit.tjv.rolihtar.shelters.persistence;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Adopter;
import org.springframework.data.repository.CrudRepository;

public interface AdopterRepository extends CrudRepository<Adopter, Integer> {
}
