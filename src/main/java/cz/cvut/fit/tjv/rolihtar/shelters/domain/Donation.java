package cz.cvut.fit.tjv.rolihtar.shelters.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.util.Objects;

@Entity(name="donations")
public class Donation extends AbstractDomain {
    private LocalDate time;

    private boolean approved;
    @ManyToOne
    @JoinColumn(name="fromDonor_id", nullable = false)
    private Donor fromDonor;
    @ManyToOne
    @JoinColumn(name="toShelter_id", nullable = false)
    private Shelter toShelter;
    private int amount; //in eur



    public Donation() {
    }
    public Donation(LocalDate time, boolean approved, Donor fromDonor, Shelter toShelter, int amount) {
        this.time = time;
        this.approved = approved;
        this.fromDonor = fromDonor;
        this.toShelter = toShelter;
        this.amount = amount;
    }
    public Donation(int id,LocalDate time, boolean approved, Donor fromDonor, Shelter toShelter, int amount) {
        this.id = id;
        this.time = time;
        this.approved = approved;
        this.fromDonor = fromDonor;
        this.toShelter = toShelter;
        this.amount = amount;
    }


    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    public LocalDate getTime() {
        return time;
    }

    public void setTime(LocalDate time) {
        this.time = time;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public Donor getFromDonor() {
        return fromDonor;
    }

    public void setFromDonor(Donor fromDonor) {
        this.fromDonor = fromDonor;
    }

    public Shelter getToShelter() {
        return toShelter;
    }

    public void setToShelter(Shelter toShelter) {
        this.toShelter = toShelter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Donation donation = (Donation) o;
        return approved == donation.approved && amount == donation.amount && time.equals(donation.time) && fromDonor.equals(donation.fromDonor) && toShelter.equals(donation.toShelter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, approved, fromDonor, toShelter, amount);
    }
}
