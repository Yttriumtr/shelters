package cz.cvut.fit.tjv.rolihtar.shelters.business;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.*;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DonationDto;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DonationRepository;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DonorRepository;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.ShelterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class DonationService extends AbstractCrudService<Donation, DonationDto, DonationRepository> {

    private ShelterRepository shelterRepository;

    private DonorRepository donorRepository;

    @Override
    public Donation create(DonationDto donationDto) {
        Optional<Shelter> shelter = shelterRepository.findById(donationDto.getShelter().getId());
        Optional<Donor> donor = donorRepository.findById(donationDto.getDonor().getId());
        if (shelter.isPresent() && donor.isPresent()) { //if shelter exists and donor exists
            Donation donation = donationDto.map();
            donation.setToShelter(shelter.get());
            donation.setFromDonor(donor.get());
            repository.save(donation);
            return donation;
        }
        return null;
    }

    @Override
    public Donation update(DonationDto donationDto, int id) {
        Optional<Shelter> shelter = shelterRepository.findById(donationDto.getShelter().getId());
        Optional<Donor> donor = donorRepository.findById(donationDto.getDonor().getId());
        if (shelter.isPresent() && donor.isPresent()) { //if shelter exists
            Donation donation = donationDto.map();
            donation.setId(id);
            donation.setFromDonor(donor.get());
            donation.setToShelter(shelter.get());
            repository.save(donation);
            return donation;
        }
        return null;
    }

}
