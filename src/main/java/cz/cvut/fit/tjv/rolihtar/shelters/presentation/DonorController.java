package cz.cvut.fit.tjv.rolihtar.shelters.presentation;



import cz.cvut.fit.tjv.rolihtar.shelters.business.DonorService;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Adopter;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Donor;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DonorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DonorController {

    @Autowired
    private DonorService donorService;


    @GetMapping("/donors")
    public List<Donor> get() {
            return donorService.get();
    }

    @PostMapping("/donors")
    public ResponseEntity<Donor> create(@RequestBody DonorDto donorDto) {
        Donor toCreate = donorService.create(donorDto);
        if(toCreate != null) {
            return new ResponseEntity<Donor>(toCreate, HttpStatus.OK);
        }
        return new ResponseEntity<Donor>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/donors/{id}")
    public ResponseEntity<Donor> update(@RequestBody DonorDto donorDto, @PathVariable Integer id) {
        Donor toUpdate = donorService.update(donorDto,id);
        if(toUpdate != null) {
            return new ResponseEntity<Donor>(toUpdate, HttpStatus.OK);
        }
        return new ResponseEntity<Donor>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/donors/{id}")
    public ResponseEntity<Donor> delete(@PathVariable Integer id) {
        Boolean toDelete = donorService.delete(id);
        if(toDelete) {
            return new ResponseEntity<Donor>(HttpStatus.OK);
        }
        return new ResponseEntity<Donor>(HttpStatus.BAD_REQUEST);
    }
    @PostMapping("donors/{id}/donations/{donation_id}")
    public ResponseEntity<Donor> addDonation(@PathVariable (value="id") Integer donorId, @PathVariable (value="donation_id") Integer donationId) {
        Boolean toAdd = donorService.addDonation(donorId,donationId);
        if(toAdd) {
            return new ResponseEntity<Donor>(HttpStatus.OK);
        }
        return new ResponseEntity<Donor>(HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("donors/{id}/donations/{donation_id}")
    public ResponseEntity<Donor> deleteDonation(@PathVariable (value="id") Integer donorId, @PathVariable (value="donation_id") Integer donationId) {
        Boolean toDelete = donorService.removeDonation(donorId,donationId);
        if(toDelete) {
            return new ResponseEntity<Donor>(HttpStatus.OK);
        }
        return new ResponseEntity<Donor>(HttpStatus.BAD_REQUEST);
    }

}
