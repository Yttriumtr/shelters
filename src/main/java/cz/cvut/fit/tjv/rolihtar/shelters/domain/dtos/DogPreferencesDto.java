package cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Adopter;

public class DogPreferencesDto {
    String size; //
    String age;
    String homeType;
    boolean children;
    int dogsEnergy; //from 1-5

    public DogPreferencesDto(String size, String age, String homeType, boolean children, int dogsEnergy) {
        this.size = size;
        this.age = age;
        this.homeType = homeType;
        this.children = children;
        this.dogsEnergy = dogsEnergy;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHomeType() {
        return homeType;
    }

    public void setHomeType(String homeType) {
        this.homeType = homeType;
    }

    public boolean isChildren() {
        return children;
    }

    public void setChildren(boolean children) {
        this.children = children;
    }

    public int getDogsEnergy() {
        return dogsEnergy;
    }

    public void setDogsEnergy(int dogsEnergy) {
        this.dogsEnergy = dogsEnergy;
    }
}
