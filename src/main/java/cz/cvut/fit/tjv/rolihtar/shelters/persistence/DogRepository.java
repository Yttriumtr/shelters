package cz.cvut.fit.tjv.rolihtar.shelters.persistence;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Dog;
import org.springframework.data.repository.CrudRepository;

public interface DogRepository extends CrudRepository<Dog, Integer> {
}
