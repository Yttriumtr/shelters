package cz.cvut.fit.tjv.rolihtar.shelters.presentation;

import cz.cvut.fit.tjv.rolihtar.shelters.business.AdopterService;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Adopter;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Dog;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.AdopterDto;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DogPreferencesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;

@RestController //has to be a spring bean
public class AdopterController {


    @Autowired
    private AdopterService adopterService;


    @GetMapping("/adopters") //gets list of adopters, for now empty list
    public List<Adopter> get() {
        return adopterService.get();
    }

    @PostMapping("/adopters") //posts Adopter
    public ResponseEntity<Adopter> create(@RequestBody AdopterDto adopterDto) {
        Adopter toCreate = adopterService.create(adopterDto);
        if(toCreate != null) {
            return new ResponseEntity<Adopter>(toCreate, HttpStatus.OK);
        }
        return new ResponseEntity<Adopter>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/adopters/{id}")
    public ResponseEntity<Adopter> update(@RequestBody AdopterDto adopterDto, @PathVariable Integer id) {
        Adopter toUpdate = adopterService.update(adopterDto,id);
        if(toUpdate != null) {
            return new ResponseEntity<Adopter>(toUpdate, HttpStatus.OK);
        }
        return new ResponseEntity<Adopter>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/adopters/{id}")
    public ResponseEntity<Adopter> delete(@PathVariable Integer id) {
        Boolean toDelete = adopterService.delete(id);
        if(toDelete) {
            return new ResponseEntity<Adopter>(HttpStatus.OK);
        }
        return new ResponseEntity<Adopter>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("adopters/{id}/dogs/{dog_id}")
    public ResponseEntity<Adopter> addDog(@PathVariable (value="id") Integer adopterId, @PathVariable (value="dog_id") Integer dogId) {
        Boolean toAdd = adopterService.addDog(adopterId,dogId);
        if(toAdd) {
            return new ResponseEntity<Adopter>(HttpStatus.OK);
        }
        return new ResponseEntity<Adopter>(HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("adopters/{id}/dogs/{dog_id}")
    public ResponseEntity<Adopter> deleteDog(@PathVariable (value="id") Integer adopterId, @PathVariable (value="dog_id") Integer dogId) {
        Boolean toDelete = adopterService.removeDog(adopterId,dogId);
        if(toDelete) {
            return new ResponseEntity<Adopter>(HttpStatus.OK);
        }
        return new ResponseEntity<Adopter>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/adopters/{id}/adoptDog") //recommends dogs which are the most suitable for you. You fill in a form (DogPreferencesDto), additional  business operation
    public LinkedHashMap<Dog, Integer> getDogsForMe(@RequestBody DogPreferencesDto dogPreferencesDto ) {
        return adopterService.pickDog(dogPreferencesDto);
    }

}
