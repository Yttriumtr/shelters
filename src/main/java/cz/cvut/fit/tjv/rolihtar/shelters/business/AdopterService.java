package cz.cvut.fit.tjv.rolihtar.shelters.business;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Adopter;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Dog;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.AdopterDto;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DogPreferencesDto;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.AdopterRepository;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service //has to be a spring bean
public class AdopterService extends AbstractCrudService<Adopter, AdopterDto, AdopterRepository> {

    @Autowired
    private DogRepository dogRepository;

    public boolean addDog(int adopterId, int dogId) {
        Optional<Adopter> adopter = repository.findById(adopterId);
        Optional<Dog> dog = dogRepository.findById(dogId);
        if (adopter.isPresent() && dog.isPresent()) {
            Adopter adopterReal = adopter.get(); //from optional to real
            adopterReal.addDog(dog.get());
            repository.save(adopterReal);
            return true;
        }
        return false;
    }

    public boolean removeDog(int adopterId, int dogId) {
        Optional<Adopter> adopter = repository.findById(adopterId);
        Optional<Dog> dog = dogRepository.findById(dogId);
        if (adopter.isPresent() && dog.isPresent()) {
            Adopter adopterReal = adopter.get();
            adopterReal.deleteDog(dog.get());
            repository.save(adopterReal);
            return true;
        }
        return false;
    }
    public LinkedHashMap<Dog,Integer> pickDog(DogPreferencesDto dogPreferencesDto) {
        Map<Dog, Integer> dogsRanking = new HashMap<>();
        for(Dog dog : dogRepository.findAll()) {
            dogsRanking.put(dog,0);
            if(dog.getSize() == dogPreferencesDto.getSize()) {
                int currentMatch = dogsRanking.get(dog);
                currentMatch++;
                dogsRanking.replace(dog,currentMatch);
            }
            if((dogPreferencesDto.getAge().equals("young") && dog.getAge() < 3)||(dogPreferencesDto.getAge().equals("mediumAged") && dog.getAge() >= 3 && dog.getAge() < 8) || (dogPreferencesDto.getAge().equals("older") && dog.getAge() >= 8)) {
                int currentMatch = dogsRanking.get(dog);
                currentMatch++;
                dogsRanking.replace(dog,currentMatch);
            }
            if((dogPreferencesDto.getHomeType() == "house")||(dogPreferencesDto.getHomeType() == "flat" && dog.getSize() != "big")) {
                int currentMatch = dogsRanking.get(dog);
                currentMatch++;
                dogsRanking.replace(dog,currentMatch);
            }
            if((dogPreferencesDto.isChildren() && dog.getSize() != "big") || (!dogPreferencesDto.isChildren())) {
                int currentMatch = dogsRanking.get(dog);
                currentMatch++;
                dogsRanking.replace(dog,currentMatch);
            }
            if(dog.getDogsEnergy() == dogPreferencesDto.getDogsEnergy()) {
                int currentMatch = dogsRanking.get(dog);
                currentMatch++;
                dogsRanking.replace(dog,currentMatch);
            }
            if(dog.getDogsEnergy() == dogPreferencesDto.getDogsEnergy() || (dog.getDogsEnergy() == dogPreferencesDto.getDogsEnergy()+1) || (dog.getDogsEnergy() == dogPreferencesDto.getDogsEnergy()-1))  {
                int currentMatch = dogsRanking.get(dog);
                currentMatch++;
                dogsRanking.replace(dog,currentMatch);
            }
            if((dogPreferencesDto.isChildren() && dog.getDogsEnergy() <= 2) || (!dogPreferencesDto.isChildren() && dog.getDogsEnergy() >3)) {
                int currentMatch = dogsRanking.get(dog);
                currentMatch++;
                dogsRanking.replace(dog,currentMatch);
            }
        }
        LinkedHashMap<Dog,Integer> sorted = new LinkedHashMap<>();
        dogsRanking.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .forEachOrdered(x -> sorted.put(x.getKey(), x.getValue()));

        return sorted;
    }
}
