package cz.cvut.fit.tjv.rolihtar.shelters.persistence;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Donation;

import org.springframework.data.repository.CrudRepository;

public interface DonationRepository extends CrudRepository<Donation,Integer> {
}
