package cz.cvut.fit.tjv.rolihtar.shelters.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
public class Shelter extends AbstractDomain {
    private String location;
    private String name;
    @OneToMany(mappedBy = "inShelter")
    private List<Dog> dogs = new ArrayList<>();
    @OneToMany(mappedBy = "toShelter")
    private List<Donation> donations = new ArrayList<>();

    public Shelter() {
    }

    public Shelter(int id,String location, String name, List<Dog> dogs, List<Donation> donations) {
        this.id = id;
        this.location = location;
        this.name = name;
        this.dogs = dogs;
        this.donations = donations;
    }

    public Shelter(String location, String name) {
        this.location = location;
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Dog> getDogs() {
        return dogs;
    }

    public void setDogs(List<Dog> dogs) {
        this.dogs = dogs;
    }


    public List<Donation> getDonations() {
        return donations;
    }

    public void setDonations(List<Donation> donations) {
        this.donations = donations;
    }
    public void addDog(Dog dog){this.dogs.add(dog);}
    public void deleteDog(Dog dog){this.dogs.remove(dog);}
    public void addDonation(Donation donation){this.donations.add(donation);}
    public void deleteDonation(Donation donation){this.donations.remove(donation);}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shelter shelter = (Shelter) o;
        return location.equals(shelter.location) && name.equals(shelter.name) && dogs.equals(shelter.dogs) && donations.equals(shelter.donations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, name, dogs, donations);
    }
}
