package cz.cvut.fit.tjv.rolihtar.shelters.persistence;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Donor;
import org.springframework.data.repository.CrudRepository;

public interface DonorRepository extends CrudRepository<Donor,Integer> {
}
