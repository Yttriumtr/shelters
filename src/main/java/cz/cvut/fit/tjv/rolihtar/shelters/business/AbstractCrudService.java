package cz.cvut.fit.tjv.rolihtar.shelters.business;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.AbstractDomain;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DomainDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractCrudService <T extends AbstractDomain,
        DtoT extends DomainDto<T>,
        TRepo extends CrudRepository<T,Integer>>{

    @Autowired
    protected TRepo repository;


    public List<T> get() {
        List<T> list = (List<T>) repository.findAll();
        return list;
    }

    public T create(DtoT dto) {
        T type = dto.map();
        repository.save(type);
        return type;
    }

    public T update(DtoT dto, int id) {
        T type = dto.map(); type.setId(id);
        if(repository.existsById(id)) {
            type = dto.map(); type.setId(id);
            repository.save(type);
        }
        return type;
    }

    public boolean delete(int id) {
        Optional<T> type = repository.findById(id);
        if (type.isPresent()) {
            repository.deleteById(id);
            return true;
        }
        return false;
    }
}
