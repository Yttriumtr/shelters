package cz.cvut.fit.tjv.rolihtar.shelters.presentation;


import cz.cvut.fit.tjv.rolihtar.shelters.business.DonationService;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Adopter;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Donation;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DonationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
public class DonationController {

    @Autowired
    private  DonationService donationService;


    @GetMapping("/donations")
    public List<Donation> get() {
        return donationService.get();
    }

    @PostMapping("/donations")
    public ResponseEntity<Donation> create(@RequestBody DonationDto donationDto) {
        Donation toCreate = donationService.create(donationDto);
        if(toCreate != null) {
            return new ResponseEntity<Donation>(toCreate,HttpStatus.OK);
        }
        return new ResponseEntity<Donation>(HttpStatus.BAD_REQUEST);
    }
    @PutMapping("/donations/{id}")
    public ResponseEntity<Donation> update(@RequestBody DonationDto donationDto, @PathVariable Integer id) {
        Donation toUpdate = donationService.update(donationDto,id);
        if(toUpdate != null) {
            return new ResponseEntity<Donation>(toUpdate, HttpStatus.OK);
        }
        return new ResponseEntity<Donation>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/donations/{id}")
    public ResponseEntity<Donation> delete(@PathVariable Integer id) {
        Boolean toDelete = donationService.delete(id);
        if(toDelete) {
            return new ResponseEntity<Donation>(HttpStatus.OK);
        }
        return new ResponseEntity<Donation>(HttpStatus.BAD_REQUEST);
    }


}

