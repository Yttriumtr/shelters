package cz.cvut.fit.tjv.rolihtar.shelters.persistence;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Shelter;
import org.springframework.data.repository.CrudRepository;

public interface ShelterRepository extends CrudRepository<Shelter,Integer> {
}
