package cz.cvut.fit.tjv.rolihtar.shelters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication //only
@EnableJpaRepositories(basePackages = {
		"cz.cvut.fit.tjv.rolihtar.shelters.persistence"
})
public class SheltersApplication {

	public static void main(String[] args) {
		SpringApplication.run(SheltersApplication.class, args);
	}

}
