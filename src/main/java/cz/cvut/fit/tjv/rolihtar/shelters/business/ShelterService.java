package cz.cvut.fit.tjv.rolihtar.shelters.business;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.*;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.ShelterDto;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DogRepository;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DonationRepository;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DonorRepository;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.ShelterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


 @Service
public class ShelterService extends AbstractCrudService<Shelter, ShelterDto, ShelterRepository> {


    private DogRepository dogRepository;



    private DonationRepository donationRepository;


    public float getAverageAge(int shelterId) {
        Optional<Shelter> shelter = repository.findById(shelterId);
        if(shelter.isPresent()) {
            List<Dog> allDogs = shelter.get().getDogs();
            if(!allDogs.isEmpty()) {
                float age = 0;
                int numOfDogs = 0;
                for(Dog dog : allDogs) {
                    age += dog.getAge();
                    numOfDogs++;
                }
                return age/numOfDogs;
            }
            return -1;
        }
        return -2;
    }

    public boolean addDog(int shelterId, int dogId) {
        Optional<Shelter> shelter = repository.findById(shelterId);
        Optional<Dog> dog = dogRepository.findById(dogId);
        if (shelter.isPresent() && dog.isPresent()) {
            Shelter shelterReal = shelter.get(); //from optional to real
            shelterReal.addDog(dog.get());
            repository.save(shelterReal);
            return true;
        }
        return false;
    }

    public boolean removeDog(int shelterId, int dogId) {
        Optional<Shelter> shelter = repository.findById(shelterId);
        Optional<Dog> dog = dogRepository.findById(dogId);
        if (shelter.isPresent() && dog.isPresent()) {
            Shelter shelterReal = shelter.get();
            shelterReal.deleteDog(dog.get());
            repository.save(shelterReal);
            return true;
        }
        return false;
    }
    /*public boolean addDonor(int shelterId, int donorId) {
        Optional<Shelter> shelter = repository.findById(shelterId);
        Optional<Donor> donor = donorRepository.findById(donorId);
        if (shelter.isPresent() && donor.isPresent()) {
            Shelter shelterReal = shelter.get(); //from optional to real
            shelterReal.(donor.get());
            repository.save(shelterReal);
            return true;
        }
        return false;
    }

    public boolean removeDonor(int shelterId, int donorId) {
        Optional<Shelter> shelter = repository.findById(shelterId);
        Optional<Donor> donor = donorRepository.findById(donorId);
        if (shelter.isPresent() && donor.isPresent()) {
            Shelter shelterReal = shelter.get();
            shelterReal.deleteDonor(donor.get());
            repository.save(shelterReal);
            return true;
        }
        return false;
    }*/
    public boolean addDonation(int shelterId, int donationId) {
        Optional<Shelter> shelter = repository.findById(shelterId);
        Optional<Donation> donation = donationRepository.findById(donationId);
        if (shelter.isPresent() && donation.isPresent()) {
            Shelter shelterReal = shelter.get(); //from optional to real
            shelterReal.addDonation(donation.get());
            repository.save(shelterReal);
            return true;
        }
        return false;
    }

    public boolean removeDonation(int shelterId, int donationId) {
        Optional<Shelter> shelter = repository.findById(shelterId);
        Optional<Donation> donation = donationRepository.findById(donationId);
        if (shelter.isPresent() && donation.isPresent()) {
            Shelter shelterReal = shelter.get();
            shelterReal.deleteDonation(donation.get());
            repository.save(shelterReal);
            return true;
        }
        return false;
    }

}
