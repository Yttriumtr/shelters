package cz.cvut.fit.tjv.rolihtar.shelters.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//Person who will adopt dogs, it has to adopt at least one dog to exist

@Entity
public class Adopter extends AbstractDomain {

    private String name;
    private String status; // like employed, student, ...
    private List<Dog> dogs; // list of dog the Adopter has adopted

    public Adopter() {
    }

    public Adopter(int id,String name, String status, List<Dog> dogs) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.dogs = new ArrayList<>();
    }

    //for map function in AdopterDto
    public Adopter(String name, String status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Dog> getDogs() {
        return dogs;
    }

    public void setDogs(List<Dog> dogs) {
        this.dogs = dogs;
    }
    public void addDog(Dog dog){this.dogs.add(dog);}
    public void deleteDog(Dog dog){this.dogs.remove(dog);}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Adopter adopter = (Adopter) o;
        return name.equals(adopter.name) && status.equals(adopter.status) && Objects.equals(dogs, adopter.dogs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, status, dogs);
    }
}
