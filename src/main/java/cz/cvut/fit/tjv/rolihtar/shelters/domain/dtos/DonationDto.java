package cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Donation;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Donor;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Shelter;

import java.time.LocalDate;

public class DonationDto implements DomainDto<Donation> {
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate time;
    private boolean approved;
    private Donor donor;
    private Shelter shelter;
    private int amount;

    public DonationDto(LocalDate time, boolean approved, Donor donor, Shelter shelter, int amount) {
        this.time = time;
        this.approved = approved;
        this.donor = donor;
        this.shelter = shelter;
        this.amount = amount;
    }
    @Override
    public Donation map() {
        return new Donation(this.time, this.approved,this.donor, this.shelter, this.amount);
    }

    public Donor getDonor() {
        return donor;
    }

    public void setDonor(Donor donor) {
        this.donor = donor;
    }

    public Shelter getShelter() {
        return shelter;
    }

    public void setShelter(Shelter shelter) {
        this.shelter = shelter;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public LocalDate getTime() {
        return time;
    }

    public void setTime(LocalDate time) {
        this.time = time;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}

