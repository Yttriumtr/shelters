package cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos;


import cz.cvut.fit.tjv.rolihtar.shelters.domain.Donor;




public class DonorDto implements DomainDto<Donor> {
    private String name;

    public DonorDto() {}

    public DonorDto(String name) {
        this.name = name;
    }
    @Override
    public Donor map() {
        return new Donor(this.name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
