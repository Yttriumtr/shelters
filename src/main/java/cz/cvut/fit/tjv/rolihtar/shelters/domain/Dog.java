package cz.cvut.fit.tjv.rolihtar.shelters.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

@Entity
public class Dog extends AbstractDomain {
    private String name;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate birth;
    private String gender;
    private String breed;
    @ManyToOne
    @JoinColumn(name="inShelter_id", nullable = false)
    private Shelter inShelter;
    private String size;
    private int dogsEnergy; //from 1-5


    public Dog() {
    }

    public Dog(String name, LocalDate birth, String gender, String breed, Shelter inShelter, String size, int dogsEnergy) {
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.breed = breed;
        this.inShelter = inShelter;
        this.size = size;
        this.dogsEnergy = dogsEnergy;
    }
    public Dog(int id,String name, LocalDate birth, String gender, String breed, Shelter inShelter, String size, int dogsEnergy) {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.breed = breed;
        this.inShelter = inShelter;
        this.size = size;
        this.dogsEnergy = dogsEnergy;
    }

    public Shelter getInShelter() {
        return inShelter;
    }

    public void setInShelter(Shelter inShelter) {
        this.inShelter = inShelter;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getDogsEnergy() {
        return dogsEnergy;
    }

    public void setDogsEnergy(int dogsEnergy) {
        this.dogsEnergy = dogsEnergy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String bread) {
        this.breed = bread;
    }

    public float getAge() {
        long days = ChronoUnit.DAYS.between(this.birth,LocalDate.now());
        float age = (float) days/365;
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return dogsEnergy == dog.dogsEnergy && name.equals(dog.name) && birth.equals(dog.birth) && gender.equals(dog.gender) && breed.equals(dog.breed) && inShelter.equals(dog.inShelter) && size.equals(dog.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birth, gender, breed, inShelter, size, dogsEnergy);
    }
}
