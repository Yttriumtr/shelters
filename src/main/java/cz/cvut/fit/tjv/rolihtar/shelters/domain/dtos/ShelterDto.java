package cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Shelter;

public class ShelterDto implements DomainDto<Shelter> {
    private String location;
    private String name;

    public ShelterDto(int id, String location, String name) {
        this.location = location;
        this.name = name;
    }
    @Override
    public Shelter map() {
        return new Shelter(this.location, this.name);
    }
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
