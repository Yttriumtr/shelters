package cz.cvut.fit.tjv.rolihtar.shelters.business;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.*;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DonorDto;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DonationRepository;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DonorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class DonorService extends AbstractCrudService<Donor,DonorDto,DonorRepository> {


    private DonationRepository donationRepository;

    public boolean addDonation(int donorId, int donationId) {
        Optional<Donor> donor = repository.findById(donorId);
        Optional<Donation> donation = donationRepository.findById(donationId);
        if (donor.isPresent() && donation.isPresent()) {
            Donor donorReal = donor.get(); //from optional to real
            donorReal.addDonation(donation.get());
            repository.save(donorReal);
            return true;
        }
        return false;
    }

    public boolean removeDonation(int donorId, int donationId) {
        Optional<Donor> donor = repository.findById(donorId);
        Optional<Donation> donation = donationRepository.findById(donationId);
        if (donor.isPresent() && donation.isPresent()) {
            Donor donorReal = donor.get();
            donorReal.deleteDonation(donation.get());
            repository.save(donorReal);
            return true;
        }
        return false;
    }

}
