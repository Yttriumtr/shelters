package cz.cvut.fit.tjv.rolihtar.shelters.domain;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
@Entity
public class Donor extends AbstractDomain {
    private String name;
    @OneToMany(mappedBy = "fromDonor")
    private List<Donation> donations = new ArrayList<>();
    //Donor can donate money to many shelters and one shelter can have many donors

    public Donor() {
    }

    public Donor(int id,String name, List<Donation> donations) {
        this.id = id;
        this.name = name;
        this.donations = new ArrayList<>();

    }

    public Donor(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void addDonation(Donation donation) {this.donations.add(donation);}
    public void deleteDonation(Donation donation) {this.donations.remove(donation);}
}
