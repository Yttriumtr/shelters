package cz.cvut.fit.tjv.rolihtar.shelters.presentation;


import cz.cvut.fit.tjv.rolihtar.shelters.business.DogService;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Dog;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DogDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
public class DogController {


    @Autowired
    private  DogService dogService;

    @GetMapping("/dogs")
    public List<Dog> get() {
        return dogService.get();
    }

    @PostMapping("/dogs")
    public ResponseEntity<Dog> create(@RequestBody DogDto dogDto) {
        Dog toCreate = dogService.create(dogDto);
        if(toCreate != null) {
            return new ResponseEntity<Dog>(toCreate, HttpStatus.OK);
        }
        return new ResponseEntity<Dog>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/dogs/{id}")
    public ResponseEntity<Dog> update(@RequestBody DogDto dogDto, @PathVariable Integer id) {
        Dog toUpdate = dogService.update(dogDto,id);
        if(toUpdate != null) {
            return new ResponseEntity<Dog>(toUpdate, HttpStatus.OK);
        }
        return new ResponseEntity<Dog>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/dogs/{id}")
    public ResponseEntity<Dog> delete(@PathVariable Integer id) {
        Boolean toDelete = dogService.delete(id);
        if(toDelete) {
            return new ResponseEntity<Dog>(HttpStatus.OK);
        }
        return new ResponseEntity<Dog>(HttpStatus.BAD_REQUEST);
    }

}
