package cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos;


import cz.cvut.fit.tjv.rolihtar.shelters.domain.Adopter;

public class AdopterDto implements DomainDto<Adopter> {
    private String name;
    private String status;

    public AdopterDto(String name, String status) {
        this.name = name;
        this.status = status;
    }
    @Override
    public Adopter map() {
        return new Adopter(this.name, this.status);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}