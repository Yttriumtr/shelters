package cz.cvut.fit.tjv.rolihtar.shelters.presentation;


import cz.cvut.fit.tjv.rolihtar.shelters.business.ShelterService;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Donor;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Shelter;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.ShelterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ShelterController {


    @Autowired
    private ShelterService shelterService;


    @GetMapping("/shelters")
    public List<Shelter> get() {
        return shelterService.get();
    }
    @PostMapping("/shelters")
    public ResponseEntity<Shelter> create(@RequestBody ShelterDto shelterDto) {
        Shelter toCreate = shelterService.create(shelterDto);
        if(toCreate != null) {
            return new ResponseEntity<Shelter>(toCreate, HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/shelters/{id}")
    public ResponseEntity<Shelter> update(@RequestBody ShelterDto shelterDto, @PathVariable Integer id) {
        Shelter toUpdate = shelterService.update(shelterDto,id);
        if(toUpdate != null) {
            return new ResponseEntity<Shelter>(toUpdate, HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("/shelters/{id}")
    public ResponseEntity<Shelter> delete(@PathVariable Integer id) {
        Boolean toDelete = shelterService.delete(id);
        if(toDelete) {
            return new ResponseEntity<Shelter>(HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }
    @PostMapping("shelters/{id}/donations/{donation_id}")
    public ResponseEntity<Shelter> addDonation(@PathVariable (value="id") Integer shelterId, @PathVariable (value="donation_id") Integer donationId) {
        Boolean toAdd = shelterService.addDonation(shelterId,donationId);
        if(toAdd) {
            return new ResponseEntity<Shelter>(HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("shelter/{id}/donations/{donation_id}")
    public ResponseEntity<Shelter> deleteDonation(@PathVariable (value="id") Integer shelterId, @PathVariable (value="donation_id") Integer donationId) {
        Boolean toDelete = shelterService.removeDonation(shelterId,donationId);
        if(toDelete) {
            return new ResponseEntity<Shelter>(HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }

   /* @PostMapping("shelters/{id}/donors/{donor_id}")
    public ResponseEntity<Shelter> addDonor(@PathVariable (value="id") Integer shelterId, @PathVariable (value="donor_id") Integer donorId) {
        Boolean toAdd = shelterService.addDonor(shelterId,donorId);
        if(toAdd) {
            return new ResponseEntity<Shelter>(HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("shelter/{id}/donors/{donor_id}")
    public ResponseEntity<Shelter> deleteDonor(@PathVariable (value="id") Integer shelterId, @PathVariable (value="donor_id") Integer donorId) {
        Boolean toDelete = shelterService.removeDonor(shelterId,donorId);
        if(toDelete) {
            return new ResponseEntity<Shelter>(HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }*/
    @PostMapping("shelters/{id}/dogs/{dog_id}")
    public ResponseEntity<Shelter> addDog(@PathVariable (value="id") Integer shelterId, @PathVariable (value="dog_id") Integer dogId) {
        Boolean toAdd = shelterService.addDog(shelterId,dogId);
        if(toAdd) {
            return new ResponseEntity<Shelter>(HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("shelter/{id}/dogs/{dog_id}")
    public ResponseEntity<Shelter> deleteDog(@PathVariable (value="id") Integer shelterId, @PathVariable (value="dog_id") Integer dogId) {
        Boolean toDelete = shelterService.removeDog(shelterId,dogId);
        if(toDelete) {
            return new ResponseEntity<Shelter>(HttpStatus.OK);
        }
        return new ResponseEntity<Shelter>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/shelters/{id}/averageAge") //average age of adopted dogs in shelter with {id}, additional complex query
    public ResponseEntity<Float> getAverageAge(@PathVariable Integer id ) {
        float averageAge = shelterService.getAverageAge(id);
        if(averageAge==-1) {
            return new ResponseEntity<Float>(HttpStatus.NO_CONTENT);
        }
        else if(averageAge==-2) {
            return new ResponseEntity<Float>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Float>(averageAge,HttpStatus.OK);
    }

}

