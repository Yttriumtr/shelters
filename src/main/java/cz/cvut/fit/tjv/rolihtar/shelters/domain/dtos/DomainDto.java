package cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos;

public interface DomainDto<T> {
    public T map();
}

