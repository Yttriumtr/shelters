package cz.cvut.fit.tjv.rolihtar.shelters.business;

import cz.cvut.fit.tjv.rolihtar.shelters.domain.Dog;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.Shelter;
import cz.cvut.fit.tjv.rolihtar.shelters.domain.dtos.DogDto;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.DogRepository;
import cz.cvut.fit.tjv.rolihtar.shelters.persistence.ShelterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DogService extends AbstractCrudService <Dog, DogDto, DogRepository> {

    private ShelterRepository shelterRepository;

    @Override
    public Dog create(DogDto dogDto) {
        Optional<Shelter> shelter = shelterRepository.findById(dogDto.getShelter().getId());
        if (shelter.isPresent()) { //if shelter exists
            Dog dog = dogDto.map();
            dog.setInShelter(shelter.get());
            repository.save(dog);
            return dog;
        }
        return null;
    }

    @Override
    public Dog update(DogDto dogDto, int id) {
        Optional<Shelter> shelter = shelterRepository.findById(dogDto.getShelter().getId());
        if (shelter.isPresent()) { //if shelter exists
            Dog dog = dogDto.map();
            dog.setId(id);
            if(repository.existsById(id)) {
                repository.save(dog);
            }
            return dog;
        }
        return null;
    }

}
